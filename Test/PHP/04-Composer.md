# Nazca Tech & Consulting

## Prueba de Desarrollo: Composer

Manejador de paquetes para PHP

## 01 - Inicio

Inicializar una configuración de composer

## 02 - Uso

Se debe instalar como dependencia la librería [Carbon](https://carbon.nesbot.com/) y en un archivo php hacer uso de sus funciones de la siguiente manera:

- Recuperar la fecha actual
- Agregar una semana a la fecha
- Restar 3 meses a la fecha
- presentar la fecha en formato Mes/Dia/Año
- Calcular el numero de dias entre tu dia de nacimiento y la fecha actual
