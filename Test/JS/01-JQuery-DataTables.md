# Nazca Tech & Consulting

## Prueba de Desarrollo: Jquery & DataTables

Front end interactivo básico

## 01 - Ajax

Usando el api [Json Placeholder](http://jsonplaceholder.typicode.com) alimentar una tabla via ajax que muestre los datos del endpoint [/user](http://jsonplaceholder.typicode.com/users)

La vista resultante debe contener de cada fila

| Atributo | Titulo |
|---|---|
|id|#|
|name|Name|
|email|Email|
|address.city|City|
|company.name|Company|

dando un resultado parecido a el siguiente

| # | Name | Email | City | Company|
|---|---|---|---|---|
|1|a_name|a_email|a_city|a_company|
|2|b_name|b_email|b_city|b_company|
|3|c_name|c_email|c_city|c_company|
